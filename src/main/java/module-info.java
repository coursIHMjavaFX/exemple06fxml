module edu.mermet {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.mermet to javafx.fxml;
    exports edu.mermet;
}
