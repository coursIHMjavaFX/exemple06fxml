package edu.mermet;

import java.io.IOException;
import java.time.LocalTime;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controleur {

	@FXML
	private Label etiquetteHeure;
	
	@FXML
	public Button bHeure;
	
	@FXML
	private void quitter() throws IOException {
		System.exit(0);
	}

	@FXML
	private void afficherHeure() throws IOException {
		LocalTime heure = LocalTime.now();
		String chaine = heure.getHour() + ":" + heure.getMinute() + ":" + heure.getSecond();
		System.out.println(chaine);
		etiquetteHeure.setText(chaine);
	}
}
